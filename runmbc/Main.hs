module Main where

import Frames
import Invariants
import Util

import Data.List
import Data.Either
import Control.Monad.Error
import System.Environment
import System.IO
import System.Directory
import System.FilePath
import Language.Eiffel.Syntax
import Language.Eiffel.Parser.Parser
import Language.Eiffel.PrettyPrint

defaultOutDir = "modified"

main = do
  dir:_ <- getArgs
  files <- filesWithExt "e" dir
  classes <- mapM parseClassFile files
  h <- openFile outFile WriteMode
  mapM (go h (rights classes)) classes
  hClose h
  where
    go h classes clsE = do
      case clsE of
        Left err -> return ()
        Right cls -> do
          let allRoutnes = concatMap routines (featureClauses cls)
          let allModifies = map (modifySpec classes cls) allRoutnes
          zipWithM_ (printOne h cls) allRoutnes allModifies
    printOne h c r mods = let 
        showMod (i, m) = (if i == 0 
          then m 
          else (declName $ routineArgs r !! (i - 1)) ++ "." ++ m) 
          ++ " "
      in if (isPublicRoutine c r || isPublicCreate c r) && not (isSpecOnly r)
        then hPutStr h $ "{" ++ className c ++ "}." ++ routineName r ++ ": " ++ concatMap showMod mods ++ "\n"
        else return ()
    outFile = "modifies"
    
-- | List of files with extension ext located (recursively) in path
filesWithExt :: String -> FilePath -> IO [FilePath]
filesWithExt ext path = do
  isDir <- doesDirectoryExist path
  if isDir 
    then do
      content <- getDirectoryContents path
      results <- mapM (filesWithExt ext) (map (combine path) (content \\ relativePaths))
      return $ concat results
    else if snd (splitExtension path) == '.':ext
      then return [path]
      else return []
  where
    relativePaths = [".", ".."]
    