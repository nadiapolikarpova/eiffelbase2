module Main where

import Language.Eiffel.Syntax
import Language.Eiffel.Parser.Parser
import Language.Eiffel.PrettyPrint
import Language.Eiffel.Util
import Language.Eiffel.Position
import Text.Parsec.ByteString
import System.Environment
import System.IO
import System.Directory
import System.FilePath

outdir = "pretty"
  
main = do
  files <- getArgs
  createDirectory outdir
  mapM_ go files
  where
    go file = do
      clsE <- parseClassFile file
      case clsE of
        Left err -> print err
        Right cls -> do
          h <- openFile (joinPath [outdir, takeFileName file]) WriteMode
          hPrint h (toDoc cls)
          hClose h