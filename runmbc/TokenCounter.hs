module Main where

import Language.Eiffel.Syntax
import Language.Eiffel.Parser
import Language.Eiffel.PrettyPrint
import Language.Eiffel.Util
import Language.Eiffel.Position
import qualified Data.HashMap.Strict as Map
import qualified Data.Set as Set
import qualified Data.Text as Text
import Data.Char (isSpace)
import Data.Maybe
import Data.List ((\\))
import Control.Lens hiding (op)
import Control.Applicative hiding (empty)
import Text.Parsec.ByteString
import Text.PrettyPrint (Doc, text, empty)
import System.Environment
import System.IO
import System.Directory
import System.FilePath

-- tempFile = "contracts.e"
  
main = do
  files <- getArgs
  mapM_ go files
  where
    go file = do
      clsE <- parseClassFile file
      case clsE of
        Left err -> print err
        Right cls -> do
          let cls' = removeGarbage cls
          putStr $ (Text.unpack $ className cls') ++ " "
          -- Method statistic:
          countMethods cls'
          -- Overhead statistics:
          let allFile = take (length file - 2) file ++ "_all.e"
          countFiltered cls' (:[]) toDoc allFile
          let execFile = take (length file - 2) file ++ "_exec.e"
          countFiltered cls' extractExecutable toDoc execFile
          -- Specification types:
          countSpecTypes cls'
          -- Interface specs:
          countInterfaceSpecs cls'          
          putStr $ "\n"
    removeGarbage cls = cls { classNote = filter isSpecNote (classNote cls) }
    
-- | Output numbers of various kinds of methods    
countMethods cls = do
  putStr $ show (countMethods isPublicNewExec) ++ " "
  putStr $ show (countMethods isExec) ++ " "
  putStr $ show (countMethods isEffectiveExec) ++ " "
  putStr $ show (countMethods isDynamic) ++ " "
  putStr $ show (countMethods isExplicitContracts) ++ " "
  putStr $ show (countMethods isExplicitWrapping) ++ " "
  where
    countMethods flt = Map.size $ Map.filterWithKey flt (view fmRoutines $ featureMap cls)    
    isPublicNewExec name (ExportedFeature exp r) = Set.null exp && not (isGhostRoutine cls name) && noPrevVersions r
    noPrevVersions r = isNewContract (routineReq r) || isNewContract (routineEns r)
    isNewContract c = (not $ null $ contractClauses c) && (not $ contractInherited c)
    isExec name _ = not (isGhostRoutine cls name)
    isEffectiveExec name (ExportedFeature _ r) = not (isGhostRoutine cls name) && case routineImpl r of
      RoutineDefer -> False
      _ -> True    
    isDynamic _ (ExportedFeature _ r) = isStatus (routineNote r) "dynamic"
    isExplicitContracts _ (ExportedFeature _ r) = isExplicit (routineNote r) "contracts"
    isExplicitWrapping _ (ExportedFeature _ r) = isExplicit (routineNote r) "wrapping"
    
-- | Output the number of public invariant clauses and mean number of public method specs    
countInterfaceSpecs cls = if deferredClass cls
  then putStr $ show (length $ invnts cls) ++ " " ++ show meanMethodSpec ++ " "
  else putStr $ ""
  where
    meanMethodSpec = (\xs -> sum xs `div` length xs) . map (go . view exportFeat) . Map.elems . Map.filterWithKey isPublicExec . view fmRoutines . featureMap $ cls    
    isPublicExec name (ExportedFeature exp _) = Set.null exp && not (isGhostRoutine cls name)
    go r = length (contractClauses $ routineReq r) + length (contractClauses $ routineEns r) 
    
-- | Output LOC and tokens of the AST of class cls filtered with flt and pretty-printed to tempFile with printer 
countFiltered cls flt printer tempFile = do  
  h <- openFile tempFile WriteMode
  let doc = vsep (map printer (flt cls))
  hPrint h doc
  hClose h
  toks <- countTokens tempFile
  -- removeFile tempFile   
  putStr $ show (locFromDoc doc) ++ " " ++ show toks ++ " "
  
locFromDoc doc = length $ filter (not . all isSpace) $ lines $ show doc
    
-- | Output LOC of various kinds of specs
countSpecTypes cls = 
  mapM (\t -> putStr $ show (countLOC t) ++ " ") [Pre, Post, Frame, RecVariant, ModelVar, ClassInv, LoopInv, LoopVariant, LoopFrame, Lemma, GhostClass, GhostFunction, GhostVar, GhostCode, Assert, Trigger, SpecNote]
  where
    countLOC specType = locFromDoc $ filterPrint specType cls
    -- countLOC specType = filterPrint specType cls
      
extractExecutable :: Clas -> [Clas]
extractExecutable cls = if isSpecClass cls
  then []
  else [cls {
    classNote = [],
    inherit = map (\inh -> inh { inheritClauses = map stripGhostInherit (inheritClauses inh) }) (inherit cls),
    featureMap = stripAttributes . stripRoutines $ featureMap cls,
    invnts = []
    }]
  where
    stripAttributes = over fmAttrs (Map.map (over exportFeat (stripAttrSpecs cls)) . (Map.filter (\er -> not (isSpecAttribute (er^.exportFeat)))))
    stripRoutines = over fmRoutines (Map.map (over exportFeat (stripRoutineSpecs cls)) . Map.filter (\er -> not (isSpecRoutine (er^.exportFeat))))
    stripGhostInherit clause = clause { 
        rename = filter (not . isGhostRename) (rename clause), 
        redefine = filter (not . isGhostFeature cls) (redefine clause),
        undefine = filter (not . isGhostFeature cls) (undefine clause)
      }
    isGhostRename r = isGhostFeature cls $ renameNew r
    
isTrue expr = case contents expr of
  LitBool True -> True
  _ -> False
  
isGhostExpr cls r expr = case contents expr of
  VarOrCall name -> isGhostFeature cls name || isGhostLocal r cls name
  UnqualCall name _ -> isGhostRoutine cls name
  QualCall e name _ -> isGhostExpr cls r e || isGhostFeature cls name
  Lookup e _ -> isGhostExpr cls r e
  BinOpExpr _ e1 e2 -> any (isGhostExpr cls r) [e1, e2]
  UnOpExpr _ e -> isGhostExpr cls r e
  Attached _ e _ -> isGhostExpr cls r e
  AcrossExpr range _ _ _ -> isGhostExpr cls r range
  Tuple es -> any (isGhostExpr cls r) es
  LitArray es -> any (isGhostExpr cls r) es
  _ -> False
    
isGhostAttribute cls name = name `elem` (builtInGhostAttributes ++ likelyGhostAttributes ++ map (declName . attrDecl) (filter isSpecAttribute $ allAttributes cls))
isGhostRoutine cls name = Text.isPrefixOf (Text.pack "lemma_") name || (name `elem` (builtInGhostRoutines ++ likelyGhostRoutines ++ map routineName (filter isSpecRoutine $ allRoutines cls)))
isGhostFeature cls name = isGhostAttribute cls name || isGhostRoutine cls name

isGhostLocal r cls name = case filter (\d -> declName d == name) (routineArgs r ++ routineDecls r) of
  [d] -> isSpecDecl cls d
  _   -> False

builtInGhostAttributes = map Text.pack ["closed", "owns", "owner", "subjects", "observers"]  
builtInGhostRoutines = map Text.pack ["wrap", "unwrap", "unwrap_no_inv", "wrap_all", "unwrap_all", "set_owns", "set_subjects", "set_observers", "use_definition"]
likelyGhostAttributes = map Text.pack ["bag", "set", "map", "sequence", "index_"]  
likelyGhostRoutines = map Text.pack ["is_model_equal", "add_iterator", "forget_iterator"]
  
isSpecNote note = noteTag note `elem` map Text.pack ["status", "explicit", "model", "guard", "manual_inv", "false_guards"]

noteHasValue notes tag str = case filter (\n -> (Text.unpack $ noteTag n) == tag) notes of
  [] -> False
  [Note _ values] -> (VarOrCall $ Text.pack str) `elem` values

isStatus notes str = noteHasValue notes "status" str  
isExplicit notes str = noteHasValue notes "explicit" str  

isSpecClass c = isStatus (classNote c) "ghost"
isSpecRoutine r = isStatus (routineNote r) "ghost" || isStatus (routineNote r) "lemma"
isSpecAttribute a = isStatus (attrNotes a) "ghost"
  
isSpecDecl cls (Decl n t) = Text.isSuffixOf (Text.pack "_") n || isSpecType cls t

isSpecType :: Clas -> Typ -> Bool
isSpecType cls (ClassType c _) = Text.isPrefixOf (Text.pack "MML_") c
isSpecType cls (Like (Pos _ (VarOrCall name))) = isGhostFeature cls name
isSpecType _ _ = False
  
  
stripAttrSpecs cls a = a {
  attrReq = Contract True [],
  attrEns = Contract True [],
  attrNotes = filter (not . isSpecNote) (attrNotes a)
}  
    
stripRoutineSpecs cls r = r {
  routineArgs = filter (not . isSpecDecl cls) (routineArgs r),
  routineReq = Contract True [],
  routineEns = Contract True [],
  routineNote = filter (not . isSpecNote) (routineNote r),
  routineImpl = stripBody $ routineImpl r 
}
  where
    stripBody impl = case impl of
      RoutineDefer -> impl
      RoutineExternal _ _ -> impl
      RoutineBody locs procs body -> RoutineBody (filter (not . isSpecDecl cls) locs) procs (stripSpecStatements cls r body)

stripSpecStatements :: Clas -> Routine -> Stmt -> Stmt
stripSpecStatements cls r (Pos p s) = Pos p $ case s of
  Check _ -> Block []
  CallStmt e -> if isGhost e then Block [] else s
  Assign lhs _ -> if isGhost lhs then Block [] else s
  Create _ lhs _ _ -> if isGhost lhs then Block [] else s
  Block stmts -> Block $ filter (not . emptyBlock) (map strip stmts)
  If cond thenBlock elifBlocks mElseBlock -> if isGhost cond
    then Block []
    else let 
      then' = strip thenBlock
      elseif' = filter (not . emptyElseIf) $ map (mapElseIf strip) elifBlocks
      mElse' = case mElseBlock of
        Nothing -> Nothing
        Just elseBlock -> let else' = strip elseBlock in
          if emptyBlock else' then Nothing else Just else'
    in if emptyBlock then' && null elseif' && isNothing mElse'
      then Block []
      else If cond then' elseif' mElse'
  Across range var _ body _ -> if isGhost range
    then Block []
    else Across range var [] (strip body) Nothing
  Loop from _ until body _ -> if isGhost until
    then Block []
    else Loop (strip from) [] until (strip body) Nothing
  _ -> s
  where
    isGhost = isGhostExpr cls r
    strip = stripSpecStatements cls r    
    emptyBlock s = contents s == Block []
    emptyElseIf (ElseIfPart c s) = emptyBlock s
    
-- | Specification types
data SpecType = Pre | Post | Frame | RecVariant | ModelVar | ClassInv | LoopInv | LoopVariant | LoopFrame | Lemma | GhostClass | GhostFunction | GhostVar | GhostCode | Assert | Trigger | SpecNote   
  deriving Eq
    
filterPrint :: SpecType -> Clas -> Doc
filterPrint specType cls = if isSpecClass cls
  then if specType == GhostClass then toDoc cls else empty
  else let internalDoc = map (filterPrintAttr specType cls) (allAttributes cls) ++ map (filterPrintRoutine specType cls) (allRoutines cls) 
    in case specType of
      ClassInv -> invars $ invnts cls
      SpecNote -> vsep $ notes (filter isSpecNote (classNote cls)) : internalDoc
      _ -> vsep $ internalDoc

filterPrintAttr :: SpecType -> Clas -> Attribute Expr -> Doc  
filterPrintAttr specType cls attr = if isSpecAttribute attr
  then case specType of 
    ModelVar -> if isModel then attrDoc False attr else empty
    GhostVar -> if isModel then empty else attrDoc False attr
    _ -> empty
  else if specType == SpecNote then notes (filter isSpecNote (attrNotes attr)) else empty
  where
    isModel = noteHasValue (classNote cls) "model" (Text.unpack $ declName $ attrDecl $ attr)

filterPrintRoutine :: SpecType -> Clas -> Routine -> Doc  
filterPrintRoutine specType cls r = if isStatus (routineNote r) "lemma"
  then case specType of
    Lemma -> wholeRoutine
    Trigger -> implDoc
    _ -> empty
  else if isStatus (routineNote r) "ghost"
    then case specType of
      GhostFunction -> if isProcedure then empty else wholeRoutine
      GhostCode -> if isProcedure then wholeRoutine else empty
      Trigger -> implDoc
      _ -> empty
    else case specType of -- Non-ghost routine
      Pre -> require $ (routineReq r) { contractClauses = filterPre $ contractClauses $ routineReq r }
      Post -> ensure (routineEns r)
      Frame -> clausesDoc $ filterFrame $ contractClauses $ routineReq r
      RecVariant -> clausesDoc $ filterVariant $ contractClauses $ routineReq r
      SpecNote -> notes (filter isSpecNote (routineNote r))
      Lemma -> empty
      GhostVar -> vsep [formArgs $ filter (isSpecDecl cls) (routineArgs r), implDoc]
      _ -> implDoc
  where
    isProcedure = routineResult r == NoType
    wholeRoutine = routineDoc routineBodyDoc rNoTriggers
    filterFrame clauses = filter (isFrame . contents . clauseExpr) clauses
    filterVariant clauses = filter (isVariant . contents . clauseExpr) clauses
    filterPre clauses = filter ((\e -> not $ isFrame e || isVariant e) . contents . clauseExpr) clauses
    isFrame (UnqualCall name _) = name `elem` map Text.pack ["modify", "modify_field", "modify_model", "reads", "reads_field", "reads_model"]
    isFrame _ = False
    isVariant (UnqualCall name _) = name == Text.pack "decreases"
    isVariant _ = False
    implDoc = filterPrintBody $ routineImpl r
    filterPrintBody impl@(RoutineBody locs _ body) = let
        ghostLocs = filter (isSpecDecl cls) locs
        bodyDoc = filterPrintStmt specType cls r body
      in case specType of
        GhostVar -> if length ghostLocs == length locs then vsep [locals impl, bodyDoc] else vsep $ (map decl ghostLocs) ++ [bodyDoc]
        _ -> bodyDoc
    filterPrintBody _ = empty
    rNoTriggers = r {
      routineImpl = stripTriggers $ routineImpl r 
    }
    stripTriggers (RoutineBody locs procs body) = RoutineBody locs procs (stripTriggersStmt body)
    stripTriggers impl = impl
    

filterPrintStmt :: SpecType -> Clas -> Routine -> Stmt -> Doc
filterPrintStmt specType cls r s = case contents s of
  Assign lhs rhs -> if specType == GhostCode && isGhost lhs then stmt s else empty
  Create _ lhs _ _ -> if specType == GhostCode && isGhost lhs then stmt s else empty
  CallStmt e -> if isOpaqueTrigger e
    then if specType == Trigger then stmt s else empty
    else if specType == GhostCode && isGhost e then stmt s else empty
  Check clauses -> let invChecks = filter (isInvCheck . contents. clauseExpr) clauses
    in case specType of
      Assert -> if length invChecks == length clauses then empty else stmt (Pos (position s) (Check $ clauses \\ invChecks)) 
      Trigger -> if length invChecks == length clauses then stmt s else vsep $ map clause invChecks  
      _ -> empty
  Block stmts -> vsep $ map go stmts
  If cond thenBlock elifBlocks mElseBlock -> if isGhost cond
    then if specType == GhostCode then stmt s else empty
    else vsep $ go thenBlock : map (go . body) elifBlocks ++ map go (maybeToList mElseBlock)  -- non-ghost if 
  Loop from invs until body var -> if isGhost until
    then if specType == GhostCode then stmt s else empty
    else case specType of
      LoopInv -> text "invariant" $?$ clausesDoc (filterInv invs)
      LoopFrame -> clausesDoc $ filterFrame invs
      LoopVariant -> vsep [text "variant" $?$ maybe empty (nestDef . expr) var, clausesDoc $ filterVariant invs]
      _ -> vsep [go from, go body]
  _ -> empty
  where
    go = filterPrintStmt specType cls r
    isGhost = isGhostExpr cls r
    body (ElseIfPart _ b) = b
    filterFrame clauses = filter (isFrame . contents . clauseExpr) clauses
    filterVariant clauses = filter (isVariant . contents . clauseExpr) clauses
    filterInv clauses = filter ((\e -> not $ isFrame e || isVariant e) . contents . clauseExpr) clauses
    isFrame (UnqualCall name _) = name `elem` map Text.pack ["modify", "modify_field", "modify_model", "reads", "reads_field", "reads_model"]
    isFrame _ = False
    isVariant (UnqualCall name _) = name == Text.pack "decreases"
    isVariant _ = False
    
stripTriggersStmt :: Stmt -> Stmt
stripTriggersStmt s = Pos (position s) $ case contents s of
  CallStmt e -> if isOpaqueTrigger e then Block [] else contents s
  Check clauses -> let clauses' = filter (not . isInvCheck . contents . clauseExpr) clauses
    in if null clauses' then Block [] else Check clauses'
  Block stmts -> Block $ filter (not . isEmptyBlock) $ map stripTriggersStmt stmts
  If cond thenBlock elifBlocks mElseBlock -> If cond (stripTriggersStmt thenBlock) (map (mapElseIf stripTriggersStmt) elifBlocks) (fmap stripTriggersStmt mElseBlock)
  Loop from invs until body var -> Loop (stripTriggersStmt from) invs until (stripTriggersStmt body) var
  _ -> contents s
  where
    isEmptyBlock (Pos _ (Block stmts)) = null stmts
    isEmptyBlock _ = False
    
isOpaqueTrigger (Pos _ (UnqualCall name _)) = name == Text.pack "use_definition"
isOpaqueTrigger _ = False

isInvCheck (VarOrCall name) = isInvCheckName name
isInvCheck (UnqualCall name _) = isInvCheckName name
isInvCheck (QualCall t name _) = isInvCheckName name || (name == Text.pack "old_" && isInvCheck (contents t))
isInvCheck _ = False
isInvCheckName name = name `elem` map Text.pack ["inv", "inv_only", "inv_without"]

mapElseIf f (ElseIfPart c s) = ElseIfPart c (f s)
    
  